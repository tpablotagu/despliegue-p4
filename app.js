const express = require("express");
const mongoose = require("mongoose");
const fileUpload = require("express-fileupload");
const bodyParser = require('body-parser');

const passport = require("passport");
const {Strategy, ExtractJwt} = require('passport-jwt');
const jwt = require("jsonwebtoken");

const tipos = require("./routes/tipos");
const inmuebles = require("./routes/inmuebles");
const principal = require("./routes/index");
const usuarios = require("./routes/usuarios");

const TipoInmueble = require("./models/tipo");
const Inmueble = require("./models/inmueble");
const Usuario = require("./models/usuario");

const Auth = require("./src/auth");
const config = require("./config");

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/inmuebles');

let app = express();

passport.use(Auth.getPassportStrategy());

app.set("view engine", "ejs");
app.set("views", __dirname + "/views");
app.use("/public", express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(fileUpload());

app.use("/", principal);
app.use("/", tipos);
app.use("/", inmuebles);
app.use("/", usuarios);

if(config.standalone){
    app.listen(5004);
} else {
    module.exports.app = app;
}

