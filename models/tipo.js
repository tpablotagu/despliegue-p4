const mongoose = require('mongoose');

let tipoSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true
    }
});

let Tipo = mongoose.model('Tipo', tipoSchema);

module.exports = Tipo;