const passport = require("passport");
const {Strategy, ExtractJwt} = require('passport-jwt');
const jwt = require('jsonwebtoken');

const Usuario = require("../models/usuario");

const config = require("../config");


class Auth {
 
    static getPassportStrategy() {
        return new Strategy({
            secretOrKey: config.jwtKey, 
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
            }, (payload, done) => {
                if (payload.login) {
                    return done(null, {login: payload.login});
                } else {
                    return done(new Error("Usuario incorrecto"), null);
                }
            });
    }

    static validLogin(login, password) {

        Usuario.find()
            .where("login").equals(login)
            .where("password").equals(password)
            .then( usuarios => { 
                if(usuarios.length > 0) {
                    return true;
                } else {
                    return false;
                }
            }).catch( error => {
                console.log(error);
                return false;
            });
    }

    static createToken(login) {
        return jwt.sign({login: login}, config.jwtKey, {expiresIn: "1 hour"});
    }
}

module.exports = Auth;