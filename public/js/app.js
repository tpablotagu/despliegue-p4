const tokenKey = "inmuebles_app_token";


$(function() {
    alternarLogin();
});

function cargar(url) {
    $('#contenido').load(url);
}

function limpiarFitros() {
    cargar("/inmuebles");
}

function eliminarInmueble(idInmueble){
    let eliminar = confirm("¿Estás seguro de que deseas eliminar el inmueble?");
    if(eliminar) {
        $.ajax({
            url:"/inmuebles/"+idInmueble,
            type:"DELETE",
            data: JSON.stringify({ id: idInmueble }),
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem(tokenKey));
            },
            success: function(data) {
                if(data.error) {
                    mostrarMensaje(data.mensajeError, "alert-danger");
                } else {
                    mostrarMensaje("Inmueble eliminado", "alert-success");

                    // $(`div[data-id='${idInmueble}'`).remove();
                    cargar("/inmuebles");
                }
            },
            error: function (data) {
                if(data.status === 401) {
                    cargar("/prohibido");
                }
            }
        });
    }
}

function filtrarListado() {
    let precio = $("#filtro_precio_input").val();
    let superficie = $("#filtro_superficie_input").val();
    let habitaciones = $("#filtro_habitaciones_input").val();

    let url = `/inmuebles?precio=${precio}&superficie=${superficie}&habitaciones=${habitaciones}`;

    cargar(url);
}

function filtarTiposInmuebles() {
    let tipo = $("#listado-tipo-inmueble").val();

    if(tipo === "0") {
        cargar("/inmuebles");
    } else {
        cargar(`/inmuebles/tipo/${tipo}`);
    }
}

function registrarse() {
    $("#form-registro").submit( function (e) {
        e.preventDefault();
    });
    let formData = new FormData(document.getElementById('form-registro'));
    $.ajax({
        url:"/registro",
        type: "POST",
        data: formData,
        contentType:false,
        cache:false,
        processData: false,
        success: function(data) {
            if(data.error) {
                mostrarMensaje(data.mensajeError, "alert-danger");
            } else {
                mostrarMensaje("Se ha completado el registro y puedes iniciar sesión", "alert-success");
                cargar("/login");
            }
            return false;
        },
        error: function (data) {
            if(data.status === 401) {
                cargar("/prohibido");
            }
        }
    });
}


function crearInmueble() {
    $("#formulario_inmueble").submit( function (e) {
        e.preventDefault();
    });

    let formData = new FormData(document.getElementById('formulario_inmueble'));
    $.ajax({
        url:"/inmuebles",
        type: "POST",
        data: formData,
        contentType:false,
        cache:false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem(tokenKey));
        },
        success: function(data) {
            if(data.error) {
                mostrarMensaje(data.mensajeError, "alert-danger");
            } else {
                mostrarMensaje("Inmueble creado con éxito", "alert-success");
                cargar("/inmuebles");
            }
            return false;
        },
        error: function (data) {
            if(data.status === 401) {
                cargar("/prohibido");
            }
        }
    });
}


function login() {
    let formLogin = $("#login").val();
    let formPassword = $("#password").val();
    let correct = true;

    if(!formLogin || formLogin ==="") {
        correct = false;
        mostrarMensaje("El usuario es necesario", "alert-danger");
        return;
    } 
    
    if(!formPassword || formPassword === "") {
        correct = false;
        mostrarMensaje("La contraseña es necesaria", "alert-danger");
        return;
    }

    if(correct) {
        $.ajax({
            url:"/login",
            type:"POST",
            data: JSON.stringify({ login: formLogin, password: formPassword }),
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            success: function(data) {
                if(data.error) {
                    mostrarMensaje("Usuario o contraseña incorrectos", "alert-danger");
                    cargar("/login");
                } else {
                    localStorage.setItem(tokenKey, data.token);
                    localStorage.setItem("usuario", data.usuario);
                    mostrarMensaje("Login correcto", "alert-success");
                    alternarLogin();
                    cargar("/inmuebles");
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
}

function mostrarMensaje(mensaje, tipo) {
    let contenedorNotificaciones = $("#notificaciones");
    
    let divAlert = document.createElement("div");

    divAlert.classList.add("alert");
    divAlert.classList.add(tipo);
    divAlert.textContent = mensaje;

    let botonCerrar = document.createElement("button");
    let spanCerrar = document.createElement("span");

    spanCerrar.setAttribute("aria-hidden", true);
    spanCerrar.textContent = "x";

    botonCerrar.setAttribute("type", "button");
    botonCerrar.setAttribute("data-dismiss", "alert");
    botonCerrar.setAttribute("aria-label","Close");
    botonCerrar.classList.add("close");

    botonCerrar.appendChild(spanCerrar);
    divAlert.append(botonCerrar);
    contenedorNotificaciones.append(divAlert);
}

function cerrarSesion() {
    localStorage.clear();
    alternarLogin();
    cargar("/inicio");
}


function alternarLogin() {
    let token = localStorage.getItem(tokenKey);
    
    let accionesLogin = $("#acciones-login");
    $("#acciones-login").children().remove();

    let elemento, elemento2, enlace, enlace2;
    if(token && token !== "") {
        elemento = document.createElement("li");
        elemento.classList.add("nav-item");
        elemento.classList.add("mr-3");
        elemento.classList.add("pt-2");
        elemento.textContent = "Hola, " + localStorage.getItem("usuario");

        elemento2 = document.createElement("li");
        enlace2 = document.createElement("a");
        elemento2.classList.add("nav-item");
        enlace2.classList.add("nav-link");
        enlace2.setAttribute("href", "javascript:cerrarSesion()");
        enlace2.textContent = "Cerrar sesión";
        elemento2.appendChild(enlace2);

        accionesLogin.append(elemento);
        accionesLogin.append(elemento2);

    } else{
        elemento = document.createElement("li");
        enlace = document.createElement("a");
        elemento.classList.add("nav-item");
        enlace.classList.add("nav-link");
        enlace.setAttribute("href", "javascript:cargar('/login')");
        enlace.textContent = "Acceder";
        elemento.appendChild(enlace);

        elemento2 = document.createElement("li");
        enlace2 = document.createElement("a");
        elemento2.classList.add("nav-item");
        enlace2.classList.add("nav-link");
        enlace2.setAttribute("href", "javascript:cargar('registro')");
        enlace2.textContent = "Registrarse";
        elemento2.appendChild(enlace2);

        accionesLogin.append(elemento);
        accionesLogin.append(elemento2);
    }
}