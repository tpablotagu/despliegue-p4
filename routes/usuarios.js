const express = require('express');
const Auth = require("../src/auth");
const bcrypt = require("bcrypt");

let router = express.Router();


const Usuario = require("../models/usuario");

router.post("/registro", (request, response) => {
    let nombre = request.body.nombre;
    let login = request.body.login;
    let password = request.body.password;
    
    if (nombre && login && password && nombre !== ""  && login !== "" && password !== "") {
        Usuario.find()
            .where("login").equals(login)
            .then( usuarios => {
                if (usuarios.length > 0) {
                    console.log("Crear usuario - usuario existente con ese login");
                    response.send({ error: true, mensajeError: "Ya existe un usuario para ese login" });
                } else {
                    bcrypt.hash(password, 10, function(err, hash) {
                        let nuevoUsuario = new Usuario({
                            nombre: nombre, login: login, password: hash
                        });
                        nuevoUsuario.save().then( resp => {
                            response.send({ error: false });
                        }).catch( error => {
                            console.log("Crear usuario - catch bcrypt");
                            console.log(error);
                            response.send({ error: true, mensajeError: "Se ha producido un error inesperado al crear el usuario" });
                        });
                    });
                }
            })
            .catch( error => {
                console.log("Crear usuario - catch comprobar usuario");
                console.log(error);
                response.send({ error: true, mensajeError: "Se ha producido un error al conectar con la base de datos" });
            });
    } else {
        console.log("Crear usuario - campos en blanco");
        response.send({ error: true, mensajeError: "Todos los campos son obligatorios" });
    }
});

router.post("/login", (request, response) => {
    let login = request.body.login;
    let password = request.body.password;

    Usuario.find()
        .where("login").equals(login)
        .then( usuarios => {
            if(usuarios && usuarios.length > 0) {
                bcrypt.compare(password, usuarios[0].password, (err, res) => {
                    if(res) {
                        let token = Auth.createToken(login);
                        response.send({error: false, token: token, usuario: usuarios[0].nombre });
                    } else{
                        response.send({error: true, mensajeError: "Login o contraseña incorrectos"});
                    }
                });
            } else {
                response.send({error: true, mensajeError: "Login o contraseña incorrectos"});
            }
        }).catch( error => {
            console.log("Login - catch buscar login");
            console.log(error);
            response.send({error: true, mensajeError: "Login o contraseña incorrectos"});
        });
});


module.exports = router;
