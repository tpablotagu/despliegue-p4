const express = require('express');
let router = express.Router();
const passport = require("passport");
const jwt = require('jsonwebtoken');

const Inmueble = require("../models/inmueble");
const Tipo = require("../models/tipo");

router.get("/inmuebles", (request, response) => {
    let precioMax = request.query.precio;
    let superficieMin = request.query.superficie;
    let habitacionesMin = request.query.habitaciones;
    let consulta = Inmueble.find();
    let filtros = {
        filtro_precio_anterior: undefined,
        filtro_superficie_anterior: undefined,
        filtro_habitaciones_anterior: undefined
    };

    if(typeof precioMax !== "undefined" && precioMax > 0) {
        consulta = consulta.where("precio").lt(precioMax);
        filtros.filtro_precio_anterior = precioMax;
    }
    if(typeof superficieMin !== "undefined" && superficieMin > 0) {
        consulta = consulta.where("superficie").gte(superficieMin);
        filtros.filtro_superficie_anterior = superficieMin;
    }
    if(typeof habitacionesMin !== "undefined" && habitacionesMin > 0 ) {
        consulta = consulta.where("habitaciones").gte(habitacionesMin);
        filtros.filtro_habitaciones_anterior = habitacionesMin;
    }
    
    consulta.populate("tipo")
    .then( inmuebles => {
        Tipo.find()
        .then( tiposInmueble => {
            response.render("listar_inmuebles", { inmuebles: inmuebles, filtros: filtros , tiposInmueble: tiposInmueble });
        })
        .catch( error => {
            console.log("ERROR: " + error);
        });       
    })
    .catch( error => {
        console.log("error obteniendo inmuebles: " + error);5
        let mensaje = {
            estilo:"alert alert-danger",
            texto: "Se ha producido un error al obtener los inmuebles"
        };  
        response.redirect("/", {  mensaje: mensaje });
    });
});

router.get("/inmuebles/tipo/:idTipo", (request, response) => {
    Inmueble.find()
        .populate("tipo")
        .then( inmuebles => {
            let returning = inmuebles.filter( x => x.tipo._id == request.params.idTipo);
            Tipo.find()
                .then( tiposInmueble => {
                    response.render("listar_inmuebles", { inmuebles: returning, tiposInmueble: tiposInmueble, tipoSeleccionado:  request.params.idTipo });
                })
                .catch( error => {
                    console.log("ERROR: " + error);
                });     
        })
        .catch( error =>  {
            response.render("listar_inmuebles");
        });
});

router.get("/inmuebles/:id", (request, response) => {
    Inmueble.findById(request.params.id)
        .populate("tipo")
        .then(inmueble => {
            if(inmueble){
                response.render('ficha_inmueble', { inmueble: inmueble });
            } else {
                response.render('ficha_inmueble', { mensaje: { estilo: "alert alert-danger", texto: "No existe el inmueble solicitado" }});
            }
        })
        .catch( error => {
            Inmueble.find()
            .populate("tipo")
            .then( inmuebles => {
                
                response.render("listar_inmuebles", { inmuebles: inmuebles, mensaje: { estilo: "alert alert-danger", texto: "No existe el inmueble solicitado" }});
            })
        });    
});


router.post("/inmuebles", passport.authenticate('jwt', {session: false, failureRedirect: ""}), (request, response) => {
    let nuevoInmueble = request.body;
    let nombreFotoInmueble = "default";
    let extension = ".jpg";

    if(request.files !== null && typeof request.files.imagen_input !== "undefined") {
        extension = request.files.imagen_input.name.substring(request.files.imagen_input.name.lastIndexOf("."));
        nombreFotoInmueble = request.files.imagen_input.md5
    } 

    nombreFotoInmueble += extension;

    let inmueble = new Inmueble({ 
        descripcion: nuevoInmueble.descripcion_input,
        tipo: nuevoInmueble.tipo_input,
        habitaciones: nuevoInmueble.habitaciones_input,
        superficie: nuevoInmueble.superficie_input,
        precio: nuevoInmueble.precio_input,
        imagen: nombreFotoInmueble
    });

    if(!inmueble.descripcion || inmueble.descripcion.length < 10) {
        return response.send({ error: true, mensajeError: "El campo 'Descripción del inmueble' debe contener al menos 10 caracteres" } );
    }
    if(!inmueble.habitaciones || inmueble.habitaciones < 1) {
        return response.send({ error: true, mensajeError: "El campo 'Número de habitaciones' no puede tener un valor inferior a 1" } );
    }
    if(!inmueble.superficie || inmueble.superficie < 25) {
        return response.send({ error: true, mensajeError: "El campo 'Superficie (m²)' no puede tener un valor inferior a 25" } );
    }
    if(!inmueble.precio || inmueble.precio < 10000) {
        return response.send({ error: true, mensajeError: "El campo 'Precio' no puede tener un valor inferior a 10000" } );
    }

    inmueble.save()
        .then( x => {
            if(request.files !== null && typeof request.files.imagen_input !== "undefined"){
                request.files.imagen_input.mv(__dirname+"/../public/uploads/" + nombreFotoInmueble, (error) => {
                    if(error !== null) {
                        response.send({ error: true, mensajeError: "Se ha producido un error al subir la imagen" } )
                    } else {
                        response.send({ error: false } )
                    }
                });
            } else {
                response.send({ error: false } )
            }
        })
        .catch( error => {
            response.send({ error: true, mensajeError: error.message } )
        });
});


router.delete("/inmuebles/:id", passport.authenticate('jwt', {session: false, failureRedirect: ""}), (request, response) => {
    let x = request.params.id;
    Inmueble.findById(request.params.id)
        .remove()
        .then(result => {
            response.send({error: false});
        }).catch( error => {
            response.send({error: false, mensajeError: error});
        }) ;
});

module.exports = router;
