const express = require('express');
let router = express.Router();


router.get("/tipos", (request, response) => {
    response.render("tipos");
});

module.exports = router;