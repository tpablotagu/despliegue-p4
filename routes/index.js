const express = require("express");
const Tipo = require("../models/tipo");
let router = express.Router();

router.get("/", (request, response) => {
    response.render("index");
});


router.get("/inicio", (request, response) => {
    response.render("inicio");
});

router.get("/nuevo_inmueble", (request, response) => {
    Tipo.find()
        .then( tiposInmueble => {
            response.render("nuevo_inmueble", { tiposInmueble: tiposInmueble });
        })
        .catch( error => {
            console.log("ERROR: " + error);

        });
});

router.get("/login", (request, response) => {
    response.render("login");
});

router.get("/registro", (request, response) => {
    response.render("registro");
});

router.get("/prohibido", (request, response) => {
    response.render("prohibido");
});




module.exports = router;